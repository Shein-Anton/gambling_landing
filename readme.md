# Структура проекта
    src
      common
        js
          main.js
        fonts
          Playfair Display
        i
          dog.jpg
        style
          common.scss
        mixins.pug
        layout.pug
      land
        i
          kitty.jpg
        style
          main.scss
        index.pug
      land
        i
          kitty.jpg
        style
          main.scss
        index.pug
    build
      land
        js
          main.js
        fonts
          Playfair Display
        i
          dog.jpg
        index.html
      land
        js
          main.js
        fonts
          Playfair Display
        i
          dog.jpg
        inde.html
    prod
      en
        land-1
          js
            main.js
          fonts
            Playfair Display
          i
            dog.jpg
          index.html
        land-2
          js
            main.js
        fonts
          Playfair Display
        i
          dog.jpg
        index.html
      ru
        land-1
          js
            main.js
          fonts
            Playfair Display
          i
            dog.jpg
          index.html
        land-2
          js
            main.js
          fonts
            Playfair Display
          i
            dog.jpg
          index.html
